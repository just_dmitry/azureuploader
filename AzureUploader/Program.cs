﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUploader
{
    class Program
    {
        static string connString = "DefaultEndpointsProtocol=http;AccountName=boccemsk;AccountKey=1jQqlw==;BlobEndpoint=http://boccemsk.blob.core.windows.net/;";
        static string container = "news";
        static string sourceFolder = @"D:\Temp\files-bocce\news\";

        static void Main(string[] args)
        {
            var storageAccount = CloudStorageAccount.Parse(connString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var blobContainer = blobClient.GetContainerReference(container);

            var di = new DirectoryInfo(sourceFolder);
            UploadFolder(di, blobContainer);
        }

        static void UploadFolder(DirectoryInfo folder, CloudBlobContainer container) {
            var files = folder.GetFiles();
            Parallel.ForEach(files, x => UploadFile(x, container));
            //foreach(var file in files) {
            //    UploadFile(file, container);
            //}

            var subdirs = folder.GetDirectories();
            foreach (var subdir in subdirs)
            {
                UploadFolder(subdir, container);
            }
        }

        static void UploadFile(FileInfo file, CloudBlobContainer container)
        {
            var name = file.FullName.Substring(sourceFolder.Length);
            name = name.Replace(file.Name, String.Join(@"\", file.Name.Split(new[] { '_' }, 2)));

            var ext = Path.GetExtension(file.Name).ToLower();
            var contentType = "application/octet-stream";
            switch (ext) {
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".zip":
                    contentType = "application/zip";
                    break;
                case ".gz":
                    contentType = "application/x-gzip";
                    break;
                case ".rar":
                    contentType = "application/x-rar-compressed";
                    break;
                case ".jpg":
                    contentType = "image/jpeg";
                    break;
                case ".gif":
                    contentType = "image/gif";
                    break;
                case ".png":
                    contentType = "image/png";
                    break;
                case ".xls":
                case ".xlsx":
                    contentType = "application/vnd.ms-excel";
                    break;
                case ".doc":
                case ".docx":
                    contentType = "application/msword";
                    break;
            }

            var blob = container.GetBlockBlobReference(name);
            blob.UploadFromStream(file.OpenRead());
            blob.Properties.ContentType = contentType;
            blob.SetProperties();
            Console.WriteLine("{0} ({1})", name, contentType);
        }
    }
}
